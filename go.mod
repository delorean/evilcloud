module evilcloud

go 1.20

require (
	github.com/boltdb/bolt v1.3.1
	github.com/gabriel-vasile/mimetype v1.4.2
	github.com/gorilla/mux v1.8.0
	github.com/landlock-lsm/go-landlock v0.0.0-20230225094210-7a98d7db83f2
	github.com/rs/zerolog v1.29.1
)

require (
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/net v0.8.0 // indirect
	golang.org/x/sys v0.6.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.66 // indirect
)
