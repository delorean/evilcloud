# evilcloud

file upload service, with a dark side.

spawns a fully functional file uploading app that logs relevant ip and browser information on whoever visits uploaded files. very effective when paired with a convincing domain (picbin.co, filebin.org, etc).

# usage

1. cloaked irc user or discord nerd says ur mom's a hoe
2. you upload a pic/video/whatever file to evilcloud
3. you send the person the link returned (ex. https://image-box.co/uploads/fj238as3.png), make them think it's some titties or something, get creative
4. victim clicks the link, thinking it's yet another image upload site, nothing to worry about
5. your console lights up with their ip and browser info paired with whatever uri was visited
6. profit

# installation

don't forget to create tls keys (ex. with letsencrypt) for the domain

```
git clone https://git.supernets.org/delorean/evilcloud
edit the global config vars in main.go to your liking
go mod tidy
go build .
./evilcloud
```


